# How to Disarm Some Common Types of Guns
*Content Warning: Discussion of Guns, Images of Guns*  
*Disclaimer: I am not a firearms professional, this document represents a layman's understanding of the subject*

## Rules for Safely Handling Guns
1. *Consider the gun dangerous at all times.* You may have unloaded a gun yourself, but you should treat it as loaded at all times as mistakes can be dangerous. All of these instructions show how to render a gun safer, not safe. Even if all the bullets have been removed from a gun, it may have sharp edges or heft that can be used to harm others.
2. *Always keep the gun pointed in a safe direction.* If you are in a group or are surrounded by people or animals, keep the gun pointed down. (But do not point it at anyone's feet.) If you are at the edge of a group or alone, it can be safe to point the gun away from others toward a concrete or brick wall or wooded area. Keep in mind that parks, golf courses, and residences may be surrounded by trees. If you do not know the area well, keep the gun pointed down. Even if a gun has been fully unloaded, pointing it at others can be seen as threatening and can put you in danger.
3. *Constantly keep a firm grip on the gun.* This will help you to follow rule #2. Only one person should handle a gun at a time. While disarming the gun, it is best to hold it as if you were firing it, with one exception.
4. *Keep your fingers off of the trigger.* If your finger is on the trigger, sudden movements or an accidental twitch of the finger may cause the gun to fire. Keep your index finger straight, pointed forward. In addition, holding your finger like this is a nearly universal signal that you do not intend to fire the gun.

## Disarm a Gun by Setting the Safety
Most guns will have a mechanical part called the "safety" or "safety catch". Generally, this is a mechanism that prevents the trigger from being pulled. Where the safety is located and what it looks like will depend on the type of gun. That being said, most guns adhere to a set of commons symbols to indicate the safety state. Remember the mnemonic phrase "red means dead". If the safety has a red indicator visible, that means the gun is "live" and can be fired. If the safety has a green or white indicator visible, that means the gun is not live and cannot be fired. Other symbols may be "S" or "SAFE" for when the safety is on and the gun cannot be fired. Some guns may have a lever-type safety, in which case the symbol pointed to by the lever or notch on the lever indicates the state of the safety mechanism. Below you can see an example safety lever circled in blue.  
![An unloaded, disarmed gun with the safety circled in blue](./pistol2.jpg "An unloaded, disarmed gun with lever-type safety circled in blue")  

## Disarm a Gun by Unloading It
Most guns store live ammunition in two places: the clip or magazine, and the breech. Bullets must be removed from both places in order to disarm a gun. To complete this process more easily, remove the magazine or clip first. To remove the clip or magazine: near the clip or magazine there will be a button or lever which must be pressed to remove it. On a pistol, this will generally be in the grip, on a rifle this will usually be somewhere in the middle of the gun, usually below the breech. While holding the button or lever down, the magazine or clip should be easily pulled out. There may be more than one button or lever, which must be pressed at the same time. To remove a bullet from the breech: pull back the slide (on a pistol, the top part which moves when firing) or bolt (the small knob which moves when firing), if the clip or magazine has been removed the slide or bolt should lock in the open position. From there, the bullet in the breech should either pop out on its own or be clearly visible, carefully remove the bullet with your fingers (if the gun has been recently fired, some of these parts may be hot). Below you can see a breech and clip release circled in blue (also note that the slide has been pulled back and is locked in an open position).  
![An unloaded, disarmed gun with the breech and clip release circled in blue](./pistol3.jpg "An unloaded, disarmed gun with the breech and clip release circled in blue")  

## Glossary
*Barrel*: The tube through which a bullet travels after being fired  
*Breech*: The area where the next bullet is placed before being fired  
*Clip*: The container where excess ammunition is stored  
*Grip*: The handle(s) where a gun is meant to be held  
*Loaded*: A gun that contains bullets  
*Safety*: A mechanism that prevents a gun's trigger from being pulled  
*Unloaded*: A gun that does not contain bullets
